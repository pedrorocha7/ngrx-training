import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './+home/home.module#HomeModule',
    data: { state: 'home'}
  },
  {
    path: 'users',
    loadChildren: './+users/users.module#UsersModule',
    data: { state: 'users'}
  },
  {
    path: 'posts',
    loadChildren: './+posts/posts.module#PostsModule',
    data: { state: 'posts'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
