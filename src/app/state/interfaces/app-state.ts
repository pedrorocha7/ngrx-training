import { User } from '../../core/models/user.model';
import { Post } from '../../core/models/post.model';

export interface AppState {
    users: User[];
    posts: Post[];
}
