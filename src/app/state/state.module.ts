import {
  NgModule,
  Optional,
  SkipSelf,
  ModuleWithProviders
}                         from '@angular/core';
import { CommonModule }   from '@angular/common';
import { StoreModule }    from '@ngrx/store';
import { EffectsModule }  from '@ngrx/effects';

import { postsReducer }   from './reducers/posts.reducer';
import { usersReducer }   from './reducers/users.reducer';
import { PostsEffects }   from './effects/posts.effects';
import { UsersEffects }   from './effects/users.effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot(
      {
        users: usersReducer,
        posts: postsReducer
      }
    ),
    EffectsModule.forRoot([UsersEffects, PostsEffects])
  ],
  declarations: []
})
export class StateModule {
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: StateModule
  ) {
    if (parentModule) {
      throw new Error(
        "StateModule is already loaded. Import it in the AppModule only"
      );
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StateModule,
      providers: []
    };
  }
}
