import * as usersActions from '../actions/users.actions';
import * as postsActions from '../actions/posts.actions';
import { User } from '../../core/models/user.model';
import { Post } from '../../core/models/post.model';

type Actions = usersActions.UsersActions | postsActions.PostsActions;
type State = User | Post;

export const deleteItem: Function = (
    state: State[],
    action: Actions
): State[] => {
    const indexToRemove: number = state.findIndex(
        (item: State) => item.id === action.payload.id
    );
    state.splice(indexToRemove, 1);
    return state;
};
