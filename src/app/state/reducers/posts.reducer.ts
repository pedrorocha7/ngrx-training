import * as postsActions from '../actions/posts.actions';
import { deleteItem } from '../utils/reducers.utils';

export const postsReducer = (
    state = [],
    action: postsActions.PostsActions
) => {
    switch (action.type) {
        case postsActions.LOAD_POSTS_SUCCESS: return action.payload;
        case postsActions.DELETE_POSTS: return state = deleteItem(state, action);
        default: return state;
    }
};
