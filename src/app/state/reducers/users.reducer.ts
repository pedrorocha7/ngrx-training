import * as usersActions from '../actions/users.actions';
import { User } from '../../core/models/user.model';
import { deleteItem } from '../utils/reducers.utils';

export const usersReducer = (
    state: User[] = [],
    action: usersActions.UsersActions
) => {
    switch (action.type) {
        case usersActions.LOAD_USERS_SUCCESS: return action.payload;
        case usersActions.DELETE_USER: return state = deleteItem(state, action);
        default: return state;
    }
};


