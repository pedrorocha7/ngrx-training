import { Action } from '@ngrx/store';
import { User } from '../../core/models/user.model';

export const LOAD_USERS = '[Users] => Load Users';
export const LOAD_USERS_SUCCESS = '[Users] => Load Users Success';
export const DELETE_USER = '[Users] => Delete User';
export const DELETE_USER_SUCCESS = '[Users] => Delete User Success';

export class LaodUsersAction implements Action {
    readonly type: string = LOAD_USERS;
    constructor(public payload: any) { }
}

export class LaodUsersSuccessAction implements Action {
    readonly type: string = LOAD_USERS_SUCCESS;
    constructor(public payload: User[]) { }
}

export class DeleteUserAction implements Action {
    readonly type: string = DELETE_USER;
    constructor(public payload: User) { }
}


export type UsersActions
    = LaodUsersAction
    | LaodUsersSuccessAction
    | DeleteUserAction;
