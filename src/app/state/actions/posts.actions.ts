import { Action } from '@ngrx/store';
import { Post } from '../../core/models/post.model';

export const LOAD_POSTS = '[Posts] => Load Posts';
export const LOAD_POSTS_SUCCESS = '[Posts] => Load Posts Success';
export const DELETE_POSTS = '[Posts] => Delete Posts';

export class LoadPostsAction implements Action {
    readonly type: string = LOAD_POSTS;
    constructor(public payload: any) { }
}

export class LoadPostsSuccessAction implements Action {
    readonly type: string = LOAD_POSTS_SUCCESS;
    constructor(public payload: Post[]) { }
}

export class DeletePostsAction implements Action {
    readonly type: string = DELETE_POSTS;
    constructor(public payload: Post) { }
}

export type PostsActions
    = LoadPostsAction
    | LoadPostsSuccessAction
    | DeletePostsAction;
