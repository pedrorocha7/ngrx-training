import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import * as usersAction from '../../state/actions/users.actions';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { UsersService } from '../../core/services/users.service';
import { User } from '../../core/models/user.model';

@Injectable()
export class UsersEffects {
    constructor(
        private usersService: UsersService,
        private actions$: Actions
    ) {}

    @Effect() loadUsers$ = this.actions$
    .ofType(usersAction.LOAD_USERS)
    .switchMap(
        (action) => this.usersService
        .getAllUsers()
        .map((users: User[]) => new usersAction.LaodUsersSuccessAction(users))
    );
}
