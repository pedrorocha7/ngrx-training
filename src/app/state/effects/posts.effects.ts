import { Injectable } from '@angular/core';
import { PostsService } from '../../core/services/posts.service';
import { Actions, Effect } from '@ngrx/effects';
import * as postsActions from '../actions/posts.actions';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import { Post } from '../../core/models/post.model';

@Injectable()
export class PostsEffects {
    constructor(
        private postsService: PostsService,
        private actions$: Actions
    ) {}

    @Effect() loadPosts$ = this.actions$
    .ofType(postsActions.LOAD_POSTS)
    .switchMap(
        () => this.postsService
        .getAllPosts()
        .map((posts: Post[]) => new postsActions.LoadPostsSuccessAction(posts))
    );
}
