import { fade } from './../../../shared/animations/fade.animation';
import {
  Component,
  ChangeDetectionStrategy
}                       from '@angular/core';
import { Store }        from '@ngrx/store';

// rxjs
import { Observable }   from 'rxjs/Observable';

// actions
import * as postsAction from '../../../state/actions/posts.actions';

import { AppState }     from '../../../state/interfaces/app-state';
import { Post }         from '../../../core/models/post.model';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [ fade() ],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[@fade]': ''
  }
})
export class IndexComponent {
  public posts$: Observable<Post[]>;

  constructor(private store: Store<AppState>) {
    this.posts$ = this.store.select((state) => state.posts);
  }

  public delete(post: Post): void {
    this.store.dispatch(new postsAction.DeletePostsAction(post));
  }

}
