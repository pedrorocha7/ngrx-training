import { NgModule }           from '@angular/core';
import { CommonModule }       from '@angular/common';

// modules
import { SharedModule }       from '../shared/shared.module';
import { PostsRoutingModule } from './posts-routing.module';

// components
import { IndexComponent }     from './containers/index/index.component';

@NgModule({
  imports: [
    CommonModule,
    PostsRoutingModule,
    SharedModule
  ],
  declarations: [IndexComponent]
})
export class PostsModule { }
