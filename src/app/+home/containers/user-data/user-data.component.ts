import { Component }    from '@angular/core';
import { Store }        from '@ngrx/store';

// rxjs
import { Observable }   from 'rxjs/Observable';

// actions
import * as usersAction from '../../../state/actions/users.actions';

import { User }         from '../../../core/models/user.model';
import { AppState }     from '../../../state/interfaces/app-state';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent {
  public users$: Observable<User[]>;

  constructor(private store: Store<AppState>) {
    this.users$ = this.store.select((state: AppState) => state.users);
  }

  public deleteUser(user: User): void {
    this.store.dispatch(new usersAction.DeleteUserAction(user));
  }

}
