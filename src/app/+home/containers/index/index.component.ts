import { fade } from './../../../shared/animations/fade.animation';
import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  animations: [ fade() ],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    '[@fade]': ''
  }
})
export class IndexComponent implements OnInit {
  public faceUrl: string;
  public dataSelectionOptions: string[];

  constructor(private router: Router) {}

  ngOnInit() {
    this.faceUrl = 'https://www.hdwallpapers.in/walls/lara_croft_tomb_raider_alicia_vikander_4k_5k-wide.jpg';
    this.dataSelectionOptions = [ 'USERS', 'POSTS' ];
  }

  routeToView(urlSegment: string): void {
    this.router.navigate([urlSegment.toLowerCase()]);
  }

}
