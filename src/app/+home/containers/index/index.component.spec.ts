import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { IndexComponent } from './index.component';
import { FaceComponent } from '../../components/face/face.component';
import { StoreModule } from '@ngrx/store';
import { HomeModule } from '../../home.module';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { DataSelectorComponent } from '../../../shared/components/data-selector/data-selector.component';
import { LogoComponent } from '../../components/logo/logo.component';

describe('IndexComponent', () => {
  let indexComponent: IndexComponent;
  let fixture: ComponentFixture<IndexComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        HomeModule,
        RouterTestingModule,
        StoreModule.forRoot({})
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(IndexComponent);
    indexComponent = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();

  }));
  /**
   * COMPONENT
   */
  it('should be created', () => expect(indexComponent).toBeTruthy());
  /**
   * NESTED COMPONENTS
   */
  // *** FaceComponent *** //
  describe('* Nested Components - FaceComponent', () => {
    let faceComponent: FaceComponent;
    let faceFixture: ComponentFixture<FaceComponent>;

    beforeEach(() => {
      faceFixture = TestBed.createComponent(FaceComponent);
      faceComponent = faceFixture.componentInstance;
      fixture.detectChanges();
    });

    it('should be created', () => expect(faceComponent).toBeTruthy());

  });
  // *** DataSelectorComponent *** //
  describe('* Nested Components - DataSelectorComponent', () => {
    let dataSelectorComponent: DataSelectorComponent;
    let dataSelectorFixture: ComponentFixture<DataSelectorComponent>;

    beforeEach(() => {
      dataSelectorFixture = TestBed.createComponent(DataSelectorComponent);
      dataSelectorComponent = dataSelectorFixture.componentInstance;
      fixture.detectChanges();
    });

    it('should be created', () => expect(dataSelectorComponent).toBeTruthy());

  });
  // *** LogoComponent *** //
  describe('* Nested Components - LogoComponent', () => {
    let logoComponent: LogoComponent;
    let logoComponentFixture: ComponentFixture<LogoComponent>;

    beforeEach(() => {
      logoComponentFixture = TestBed.createComponent(DataSelectorComponent);
      logoComponent = logoComponentFixture.componentInstance;
      fixture.detectChanges();
    });

    it('should be created', () => expect(logoComponent).toBeTruthy());

  });

});
