import { Component }    from '@angular/core';
import { Store }        from '@ngrx/store';

// rxjs
import { Observable }   from 'rxjs/Observable';

// actions
import * as postsAction from '../../../state/actions/posts.actions';

import { Post }         from '../../../core/models/post.model';
import { AppState }     from '../../../state/interfaces/app-state';

@Component({
  selector: 'app-post-data',
  templateUrl: './post-data.component.html',
  styleUrls: ['./post-data.component.scss']
})
export class PostDataComponent {
  public posts$: Observable<Post[]>;

  constructor(private store: Store<AppState>) {
    this.posts$ = this.store.select((state: AppState) => state.posts);
  }

  public deletePost(post: Post): void {
    this.store.dispatch(new postsAction.DeletePostsAction(post));
  }

}
