import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoComponent } from './logo.component';

describe('LogoComponent', () => {
  let component: LogoComponent;
  let fixture: ComponentFixture<LogoComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ LogoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));
  /**
   * COMPONENT
   */
  it('should be created', () => expect(component).toBeTruthy());

});
