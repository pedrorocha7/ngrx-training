import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaceComponent } from './face.component';
import { DebugElement } from '@angular/core';

describe('FaceComponent', () => {
  let faceComponent: FaceComponent;
  let fixture: ComponentFixture<FaceComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaceComponent ]
    })
    .compileComponents();

    fixture       = TestBed.createComponent(FaceComponent);
    faceComponent = fixture.componentInstance;
    debugElement  = fixture.debugElement;
    fixture.detectChanges();

  }));
  /**
   * COMPONENT
   */
  it('should be created', () => expect(faceComponent).toBeTruthy());
  /**
   * @Input() image
   */
  describe('* @Input() image: string', () => {

    it('should be url', () => {
      const urlRegExp: RegExp = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
      const newImageValue = 'https://www.hdwallpapers.in/walls/lara_croft_tomb_raider_alicia_vikander_4k_5k-wide.jpg';
      const imageElement = debugElement.nativeElement.querySelector('div.wrapper>img');
      // assign new value to image prop
      faceComponent.image = newImageValue;
      fixture.detectChanges();
      // check if img scr attr has been updated
      expect(urlRegExp.test(imageElement.src)).toBeTruthy();
    });

    it('should only accept urls', () => {
      const urlRegExp: RegExp = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
      const newImageValue = 'this is not a url';
      const imageElement = debugElement.nativeElement.querySelector('div.wrapper>img');
      // assign new value to image prop
      faceComponent.image = newImageValue;
      fixture.detectChanges();
      // check if img scr attr has been updated
      expect(urlRegExp.test(imageElement.src)).toBeFalsy();
    });

    it('<img /> src should have "image" property', () => {
      const newImageValue = 'https://www.hdwallpapers.in/walls/lara_croft_tomb_raider_alicia_vikander_4k_5k-wide.jpg';
      const imageElement = debugElement.nativeElement.querySelector('div.wrapper>img');
      // assign new value to image prop
      faceComponent.image = newImageValue;
      fixture.detectChanges();
      // check if img scr attr has been updated
      expect(imageElement.src).toContain(newImageValue);
    });

  });

});
