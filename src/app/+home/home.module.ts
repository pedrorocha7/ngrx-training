import { NgModule }          from '@angular/core';
import { CommonModule }      from '@angular/common';

// modules
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule }      from '../shared/shared.module';

// components
import { IndexComponent }    from './containers/index/index.component';
import { FaceComponent }     from './components/face/face.component';
import { LogoComponent }     from './components/logo/logo.component';
import { UserDataComponent } from './containers/user-data/user-data.component';
import { PostDataComponent } from './containers/post-data/post-data.component';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ],
  declarations: [
    IndexComponent,
    FaceComponent,
    LogoComponent,
    UserDataComponent,
    PostDataComponent
  ]
})
export class HomeModule { }
