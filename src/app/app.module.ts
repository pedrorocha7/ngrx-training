import 'hammerjs';

import { BrowserModule }           from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule }                from '@angular/core';
import { HttpClientModule }        from '@angular/common/http';

import { AppRoutingModule }        from './app-routing.module';
import { AppComponent }            from './app.component';
import { SharedModule }            from './shared/shared.module';
import { CoreModule }              from './core/core.module';
import { StateModule }             from './state/state.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule.forRoot(),
    StateModule.forRoot(),
    HttpClientModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
