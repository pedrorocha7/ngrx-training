import {
    trigger,
    transition,
    style,
    animate,
    state,
    AnimationTriggerMetadata,
    keyframes
} from '@angular/animations';


// *** Fade Animation Function *** //
export const fade = (
    fadeInDuration: number = 1000,
    fadeOutDuration: number = 500
): AnimationTriggerMetadata => {
    return trigger(
        'fade',
        [
            state(
                'void',
                style({ opacity: 0, transform: 'translateY(-30px)' })
            ),
            /**
             * FADE IN
             */
            transition(
                ':enter',
                [
                    animate(
                        `${fadeInDuration}ms ease-out`,
                        keyframes(
                            [
                                style({ opacity: .5, transform: 'translateY(30px)', offset: 0.5 }),
                                style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 })
                            ]
                        )
                    )
                ]
            ),
            /**
             * FADE OUT
             */
            transition(
                ':leave',
                [
                    animate(
                        `${fadeOutDuration}ms ease-out`,
                        style({ transform: 'translateY(-100px)',  opacity: 0 })
                    )
                ]
            )
        ]
    );
};
