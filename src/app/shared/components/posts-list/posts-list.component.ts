import {
  Component,
  EventEmitter,
  Output,
  Input
}                     from '@angular/core';

// rxjs
import { Observable } from 'rxjs/Observable';

import { Post }       from '../../../core/models/post.model';
import { fade }       from '../../animations/fade.animation';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
  animations: [ fade() ]
})
export class PostsListComponent {
  @Input() public posts: Observable<Post[]>;
  @Output() public delete: EventEmitter<Post> = new EventEmitter<Post>();
}
