import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';

// modules
import { MaterialModule }           from '../material/material.module';

// components
import { DataSelectorComponent }    from './data-selector/data-selector.component';
import { SelectorButtonComponent }  from './selector-button/selector-button.component';
import { NavComponent }             from './nav/nav.component';
import { UsersListComponent }       from './users-list/users-list.component';
import { PostsListComponent }       from './posts-list/posts-list.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    NavComponent,
    DataSelectorComponent,
    SelectorButtonComponent,
    UsersListComponent,
    PostsListComponent
  ],
  exports: [
    NavComponent,
    DataSelectorComponent,
    SelectorButtonComponent,
    UsersListComponent,
    PostsListComponent
  ]
})
export class ComponentsModule { }
