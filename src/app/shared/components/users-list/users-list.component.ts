import { Component, Input, Output, EventEmitter } from '@angular/core';

// rxjs
import { Observable } from 'rxjs/Observable';

import { User } from '../../../core/models/user.model';
import { fade } from '../../animations/fade.animation';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  animations: [ fade() ]
})
export class UsersListComponent {
  @Input() public users: Observable<User[]>;
  @Output() public delete: EventEmitter<User> = new EventEmitter<User>();
}
