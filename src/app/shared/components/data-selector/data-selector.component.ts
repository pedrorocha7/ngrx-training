import {
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

@Component({
  selector: 'app-data-selector',
  templateUrl: './data-selector.component.html',
  styleUrls: ['./data-selector.component.scss']
})
export class DataSelectorComponent {
  @Input() public data: string[];
  @Output() public dataSelected: EventEmitter<string> = new EventEmitter<string>();
}
