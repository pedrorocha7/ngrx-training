import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-selector-button',
  templateUrl: './selector-button.component.html',
  styleUrls: ['./selector-button.component.scss']
})
export class SelectorButtonComponent {
  @Input() public buttonText: string;
  @Output() public selected: EventEmitter<string> = new EventEmitter<string>();
}
