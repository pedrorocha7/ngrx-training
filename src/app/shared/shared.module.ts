import { NgModule }         from '@angular/core';
import { CommonModule }     from '@angular/common';

// modules
import { MaterialModule }   from './material/material.module';
import { ComponentsModule } from './components/components.module';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    MaterialModule
  ],
  exports: [
    ComponentsModule,
    MaterialModule
  ],
  declarations: [],
  providers: []
})
export class SharedModule { }
