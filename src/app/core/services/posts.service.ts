import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// ngrx
import { Observable } from 'rxjs/Observable';

import { Post } from '../models/post.model';

@Injectable()
export class PostsService {

  constructor(private http: HttpClient) { }

  public getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts');
  }

}
