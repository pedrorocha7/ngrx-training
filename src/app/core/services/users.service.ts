import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// ngrx
import { Observable } from 'rxjs/Observable';
import 'rxjs/operator/do';

import { User } from '../models/user.model';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient) { }

  public getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>('https://jsonplaceholder.typicode.com/users');
  }

}
