import { NgModule }           from '@angular/core';
import { CommonModule }       from '@angular/common';

// modules
import { UsersRoutingModule } from './users-routing.module';
import { SharedModule }       from '../shared/shared.module';

// components
import { IndexComponent }     from './containers/index/index.component';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule
  ],
  declarations: [IndexComponent]
})
export class UsersModule { }
