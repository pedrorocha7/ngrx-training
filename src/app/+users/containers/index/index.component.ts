import { fade } from './../../../shared/animations/fade.animation';
import {
  Component,
  ChangeDetectionStrategy
}                       from '@angular/core';
import { Store }        from '@ngrx/store';

// rxjs
import { Observable }   from 'rxjs/Observable';

// actions
import * as usersAction from '../../../state/actions/users.actions';

import { AppState }     from '../../../state/interfaces/app-state';
import { User }         from '../../../core/models/user.model';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexComponent {
  public users$: Observable<User[]>;

  constructor(private store: Store<AppState>) {
    this.users$ = this.store.select((state) => state.users);
  }

  public delete(user: User): void {
    this.store.dispatch(new usersAction.DeleteUserAction(user));
  }

}
