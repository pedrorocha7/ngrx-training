import { routerTransition } from './shared/animations/router.animation';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

// actions
import * as usersAction from './state/actions/users.actions';
import * as postsAction from './state/actions/posts.actions';

import { AppState } from './state/interfaces/app-state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ routerTransition ]
})
export class AppComponent {
  constructor(private store: Store<AppState>) {
    store.dispatch(new usersAction.LaodUsersAction(null));
    store.dispatch(new postsAction.LoadPostsAction(null));
  }

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}
