import { TestBed, async }      from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreModule, Store }  from '@ngrx/store';

import { AppComponent }        from './app.component';

describe('AppComponent', () => {
  let app;
  let fixture;
  let store;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        StoreModule.forRoot({})
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app     = fixture.debugElement.componentInstance;
    store   = fixture.debugElement.injector.get(Store);

  }));
  /**
   * COMPONENT
   */
  it('should be created', async(() => expect(app).toBeTruthy()));
  /**
   * STORE
   */
  describe('* Store', () => {

    it('to be defined', async(() => {
      expect(store).toBeDefined();
    }));

  });
});
